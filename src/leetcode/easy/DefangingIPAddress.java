package leetcode.easy;
/*
Given a valid (IPv4) IP address, return a defanged version of that IP address.

        A defanged IP address replaces every period "." with "[.]".



        Example 1:

        Input: address = "1.1.1.1"
        Output: "1[.]1[.]1[.]1"
        Example 2:

        Input: address = "255.100.50.0"
        Output: "255[.]100[.]50[.]0"
*/


import java.util.ArrayList;
import java.util.List;

public class DefangingIPAddress {
    public String defangIPaddr(String address) {
        String newaddress = address.replace(".","[.]");
        return newaddress;
 /*
    public String defangIPaddr(String address) {
        StringBuilder str = new StringBuilder();
        int end = address.length()
        for (int i = 0; i < end; i++){
            char curr = address.charAt(i);
            if (curr == '.'){
                str.append("[.]");
            } else {
                str.append(curr);
            }
        }
        return str.toString();
    }
 * */
    }


    public static void main(String[] args) {
        String address = "255.100.50.0";
        DefangingIPAddress de = new DefangingIPAddress();
        System.out.println(de.defangIPaddr(address));
    }
}
