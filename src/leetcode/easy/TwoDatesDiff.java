package leetcode.easy;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDate.parse;

public class TwoDatesDiff {

    public static void main (String args[]){
        String date = "20191223";
        int year =Integer.parseInt(date.substring(0,4));
        int month =Integer.parseInt(date.substring(5,6));
        int day =Integer.parseInt(date.substring(7,8));
        System.out.println(year);
        System.out.println(month);
        System.out.println(day);
        LocalDate t24date = LocalDate.of(year, month, day);
        LocalDate now = LocalDate.now();
        Period diff = Period.between(t24date, now);
        System.out.printf("Difference is %d years, %d months and %d days old",
                diff.getYears(), diff.getMonths(), diff.getDays());

    }
}
