package leetcode.easy;



public class RemoveDuplicate {
    public int removeDuplicates(int[] nums) {
        int count = 1;
        for (int i = 0; i < nums.length-1; i++) {
            if(nums[i] != nums[i+1]){
                nums[count] = nums[i+1];
                count +=1;
            }
        }
        return count;
    }




    public static void main (String[] args){
        int nums [] = {1,1,2,33,33,33,43,42};
        RemoveDuplicate s = new RemoveDuplicate();
        int result = s.removeDuplicates(nums);

            System.out.println(result);

    }
}
