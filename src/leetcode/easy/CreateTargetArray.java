package leetcode.easy;

import java.util.ArrayList;

/**
 Given two arrays of integers nums and index. Your task is to create target array under the following rules:

 Initially target array is empty.
 From left to right read nums[i] and index[i], insert at index index[i] the value nums[i] in target array.
 Repeat the previous step until there are no elements to read in nums and index.
 Return the target array.

 It is guaranteed that the insertion operations will be valid.



 Example 1:

 Input: nums = [0,1,2,3,4], index = [0,1,2,2,1]
 Output: [0,4,1,3,2]
 Explanation:
 nums       index     target
 0            0        [0]
 1            1        [0,1]
 2            2        [0,1,2]
 3            2        [0,1,3,2]
 4            1        [0,4,1,3,2]
 Example 2:

 Input: nums = [1,2,3,4,0], index = [0,1,2,3,0]
 Output: [0,1,2,3,4]
 Explanation:
 nums       index     target
 1            0        [1]
 2            1        [1,2]
 3            2        [1,2,3]
 4            3        [1,2,3,4]
 0            0        [0,1,2,3,4]
 Example 3:

 Input: nums = [1], index = [0]
 Output: [1]
 */





public class CreateTargetArray {


    public int[] createTargetArray(int[] nums, int[] index) {
        ArrayList<Integer> arrlist = new ArrayList<Integer>();
        for (int i = 0; i < nums.length ; i++) {
            arrlist.add(index[i],nums[i]);
        }
        int [] target = new int[nums.length];
        for (int i = 0; i < nums.length ; i++) {
            target[i] = arrlist.get(i);
        }
        return target;
    }



    public static void main (String[] args){
        int nums [] = {1,2,3,4,0};
        int index [] = {0,1,2,3,0};
        CreateTargetArray s = new CreateTargetArray();
        int [] result = s.createTargetArray(nums,index);
        for (int i = 0; i <result.length ; i++) {
            System.out.println(result[i]);
        }
    }
}
