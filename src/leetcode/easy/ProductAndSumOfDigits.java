package leetcode.easy;
/*
Given an integer number n, return the difference between the product of its digits and the sum of its digits.


Example 1:

Input: n = 234
Output: 15
Explanation:
Product of digits = 2 * 3 * 4 = 24
Sum of digits = 2 + 3 + 4 = 9
Result = 24 - 9 = 15
Example 2:

Input: n = 4421
Output: 21
Explanation:
Product of digits = 4 * 4 * 2 * 1 = 32
Sum of digits = 4 + 4 + 2 + 1 = 11
Result = 32 - 11 = 21
* */
public class ProductAndSumOfDigits {
    public int subtractProductAndSum(int n) {
        int digit = 0;
        int multi=1;
        int sum=0;
        for (int i = 0; i < 5 && n > 0; i++) {
            digit = n%10;
            n = n/10;
            sum = sum+digit;
            multi = multi * digit;
        }
        return  multi - sum;
    }

    public static void main (String[] args){
        int nums = 234;
        ProductAndSumOfDigits s = new ProductAndSumOfDigits();
        System.out.println(s.subtractProductAndSum(nums));
    }
}
