package leetcode.easy;

/*
*
* Balanced strings are those who have equal quantity of 'L' and 'R' characters.

Given a balanced string s split it in the maximum amount of balanced strings.

Return the maximum amount of splitted balanced strings.



Example 1:

Input: s = "RLRRLLRLRL"
Output: 4
Explanation: s can be split into "RL", "RRLL", "RL", "RL", each substring contains same number of 'L' and 'R'.
Example 2:

Input: s = "RLLLLRRRLR"
Output: 3
Explanation: s can be split into "RL", "LLLRRR", "LR", each substring contains same number of 'L' and 'R'.
Example 3:

Input: s = "LLLLRRRR"
Output: 1
Explanation: s can be split into "LLLLRRRR".
*
*
*
* Hint
Loop from left to right maintaining
a balance variable when it gets an L increase it by one otherwise decrease it by one.
* */



public class BalancedString {


    public int balancedStringSplit(String s) {
        char firstchar =  s.charAt(0);
        int count =0;
        int set=0;
        for (int i = 0; i < s.length() ; i++) {
            if (firstchar == s.charAt(i)){
                count +=1;
            } else {
                count -=1;
            }
            if(count == 0){
                set +=1;
            }
        }
        return set;
    }




    public static void main (String[] args){
        String letters = "LLLLRRRR";
        BalancedString s = new BalancedString();
        System.out.println(s.balancedStringSplit(letters));
    }
}
