package leetcode.easy;
/*
*
Example 1:

Input: nums = [12,345,2,6,7896]
Output: 2
Explanation:
12 contains 2 digits (even number of digits).
345 contains 3 digits (odd number of digits).
2 contains 1 digit (odd number of digits).
6 contains 1 digit (odd number of digits).
7896 contains 4 digits (even number of digits).
Therefore only 12 and 7896 contain an even number of digits.
Example 2:

Input: nums = [555,901,482,1771]
Output: 1
Explanation:
Only 1771 contains an even number of digits.

* */
public class EvenNumberOfDigits {

    public int findNumbers(int[] nums) {
        int number=0;
        int count=0;

        for (int i = 0; i < nums.length ; i++) {
            number = nums[i];
            int j = 0;
            while(j < 5 && number !=0){
                number /=10;
                j++;
                if(number == 0 && j%2 ==0){
                    count +=1;
                    break;
                }
            }
        }
    return count;
    }



    public static void main (String[] args){
        int nums [] = {12,345,2,6,7896};
        EvenNumberOfDigits s = new EvenNumberOfDigits();
        System.out.println(s.findNumbers(nums));
    }
}
